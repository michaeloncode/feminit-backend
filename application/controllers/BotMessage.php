<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 06/11/2018
 * Time: 13:26
 */
require(APPPATH . '/libraries/REST_Controller.php');
class BotMessage extends \Restserver\Libraries\REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('botMessage_model');
    }

    public function all_get()
    {
        $data = $this->botMessage_model->getAll();
        if (!empty($data)) {
            $this->response([
                'status' => true,
                'data' => $data
            ]);
        }
        $this->response([
            'status' => false,
        ]);

    }

    public function add_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($dataToBeInserted=[
            'key'=>$this->post('keyword'),
            'value'=>$this->post('message'),
        ]);
        setFormValidationRules([
            [
                'name'=>'key',
                'label'=>'Mot clé',
                'rules'=>'trim|required'
            ],
            [
                'name'=>'value',
                'label'=>'Message',
                'rules'=>'trim|required'
            ]
        ]);
        if($this->form_validation->run()){
            $messageID = $this->botMessage_model->insert($dataToBeInserted);
            if($messageID){
                echoResponse([
                    'status'=>true,
                    'data'=>$messageID,
                    'message'=>'Message ajouté avec succès'
                ]);
            }
            echoResponse([
                'status'=>false,
                'message'=>'Erreur rencontrée. Veuillez réessayer'
            ]);
        }
        $errorMessage = setErrorDelimiter();
        $this->response([
            'status'=>false,
            'message'=>$errorMessage
        ]);

    }

    //working with formdata
    public function retrieveByID_get(){
        $this->load->library('form_validation');
        $this->form_validation->set_data([
            'botMessageID'=>$messageID=$this->get('botMessageID')
        ]);
        setFormValidationRules([
            [
                'name'=>'botMessageID',
                'label'=>'ID Message',
                'rules'=>'trim|required|is_natural_no_zero'
            ]
        ]);
        if($this->form_validation->run()){
            $botMessage = $this->botMessage_model->get($messageID);
            if(!empty($botMessage)){
                $this->response([
                    'status'=>true,
                    'data'=>$botMessage,
                ]);
            }

        }
        $this->response([
            'status'=>false,
            'message'=>'Erreur rencontrée. Veuillez réessayer'
        ]);
    }

    //wworking with formdata
    public function deleteByID_get(){
        $this->load->library('form_validation');
        $this->form_validation->set_data([
            'messageID'=>$messageID=$this->get('messageID')
        ]);
        setFormValidationRules([
            [
                'name'=>'messageID',
                'label'=>'ID',
                'rules'=>'trim|required|is_natural_no_zero'
            ]
        ]);
        if($this->form_validation->run()){
            $this->botMessage_model->delete($messageID);
            $this->response([
                'status'=>true,
                'message'=>'Message supprimé avec succès'
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }

    //working with formdata
    public function edit_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data=[
            'id'=>$this->post('id'),
            'key'=>$this->post('keyword'),
            'value'=>$this->post('message'),
        ]);
        setFormValidationRules([
            [
                'name'=>'id',
                'label'=>'ID',
                'rules'=>'trim|required|is_natural',
            ],
            [
                'name'=>'key',
                'label'=>'Mot clé',
                'rules'=>'trim|required',
            ],
            [
                'name'=>'value',
                'label'=>'Message',
                'rules'=>'trim|required',
            ],
        ]);
        if($this->form_validation->run()){
            $this->botMessage_model->update($data['id'], $data);
            $this->response([
                'status'=>true,
                'message'=>'Message mis à jour avec succès'
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }
}