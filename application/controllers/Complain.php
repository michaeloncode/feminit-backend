<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 09/11/2018
 * Time: 19:54
 */

class Complain extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('complain_model');
    }

    public function get(){
        //var_dump_pre($this->complain_model->getAll());exit;
        echoResponse([
            'status'=>1,
            'message'=>'Fetched',
            'data'=>$this->complain_model->getAll()
        ]);
    }

    public function getByID(){
        if($complainID = $this->input->get('complainID')){
           $complainData = $this->complain_model->getByID((int) $complainID);
           if(!empty($complainData)){
               echoResponse([
                   'status'=>1,
                   'data'=>$complainData,
                   'message'=>'Fetched'
               ]);
           }
        }
        echoResponse([
            'status'=>0,
            'message'=>'Erreur rencontrée. Veuillez réessayer plus tard'
        ]);
    }

    public function getModeratorUpdate() {

        if(($complainID = (int) $this->input->get('complainID')) && ($moderatorID = $this->input->get('moderatorID'))){
           $update = $this->complain_model->getUpdateByModeratorIDAndComplainID($complainID, $moderatorID);
           if(!empty($update)){
               echoResponse([
                   'status'=>1,
                   'data'=>$update,
                   'message'=>'Fetched'
               ]);
           }
        }
        echoResponse([
            'status'=>0,
            'message'=>'Erreur rencontrée. Veuillez réessayer plus tard'
        ]);
    }

    public function setModeratorUpdate(){
       // echoResponse($this->input->post());
        if(($complainID = (int) $this->input->post('complainID')) && ($userID = (int) $this->input->post('userID'))){
            if($files=maybe_null_or_empty($_FILES, 'files')){
              // echoResponse($files['name']);
                $this->load->helper(['url']);
                $uploadedData=[];
                if(!empty($files)){
                    $limit = count($files['name']);
                    for($i=0; $i<$limit; $i++){
                        $_FILES['file']['name'] = $files['name'][$i];
                        $_FILES['file']['type'] = $files['type'][$i];
                        $_FILES['file']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['file']['error'] = $files['error'][$i];
                        $_FILES['file']['size'] = $files['size'][$i];
                        $uploadedData[] = upload_data(array(
                            'upload_path' => FCPATH . 'uploads',
                            'allowed_types' => 'jpg|png|jpeg|mp3|pdf|mp4|docx|doc|txt',
                            'max_size' => 50000), 'file');
                    }
                }
                if(!empty($uploadedData)){
                    foreach ($uploadedData as $datum){
                        $dataToBeUpdated = [
                            'complain_id'=>$complainID,
                            'user_id'=>$userID,
                            'link'=>$datum['file_name'],
                            'created_at'=>date('Y-m-d G:i:s')
                        ];
                       // echoResponse($dataToBeUpdated);
                        $this->complain_model->insertUpload($dataToBeUpdated);
                    }
                }
            }
            if($description = $this->input->post('description')){
                $this->complain_model->insertORUpdateComplainUpdate($complainID, $userID, $description);
            }
            echoResponse([
                'status'=>1,
                'message'=>'Plainte mise à jour avec succès'
            ]);
        }
        echoResponse([
            'status'=>0,
            'message'=>'Erreur rencontrée. Veuillez réessayer plus tard'
        ]);
    }

    public function uploads(){
        $this->complain_model->getOneUploadedImageOrFile(4);
    }


}