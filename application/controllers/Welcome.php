<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    $this->load->helper(['url', 'form']);
	    $data['assetsUrl']=base_url('assets/');
	    $this->load->model('complain_model');
	    $data['complainTypes']=$this->complain_model->getAllComplainTypes(true);
	    if($complain = $this->input->post('complain')){
	        $this->load->library('form_validation');
	        $this->form_validation->set_rules('complain[last_name]', 'Nom', 'trim|required');
	        $this->form_validation->set_rules('complain[first_name]', 'Prénom', 'trim|required');
	        $this->form_validation->set_rules('complain[email]', 'Email', 'trim|required');
	        $this->form_validation->set_rules('complain[age]', 'Age', 'trim|required');
	        $this->form_validation->set_rules('complain[phone]', 'Phone', 'trim|required');
	        $this->form_validation->set_rules('complain[complain_type_id]', 'Type de plainte', 'trim|required');
	        $this->form_validation->set_rules('complain[text]', 'Description', 'trim|required');
	        if($this->form_validation->run()){
                $complain['source']='web';
                $userData = [
                    'first_name'=>$complain['first_name'],
                    'last_name'=>$complain['last_name'],
                    'email'=>$complain['email'],
                ];
                unset($complain['first_name'], $complain['last_name']);
                $this->load->model('user_model');
                $userID = $this->user_model->getIDByMail($complain['email']);
                if(!$userID){
                    $insertData = $this->user_model->insert($userData);
                    $userID = maybe_null_or_empty($insertData, 'id');
                }
                $complain['member_id']=$userID;
                $this->complain_model->insert($complain);
                redirect(site_url('welcome/success'));
            }else{
                redirect(site_url('welcome/success'));
            }

        }
		$this->load->view('welcome_message', $data);
	}

	public function success(){
        $this->load->helper(['url']);
        $data['assetsUrl']=base_url('assets/');
        $this->load->view('success', $data);
    }
}
